''' The collection of tests for jsonline module
'''
import pathlib
import pytest

from sqlite_keeper.formats.jsonline import JSONLineFile


def test_formats_jsonline_load():
    ''' test formats jsonline load
    '''
    data = list(JSONLineFile.load(pathlib.Path('tests/resources/data1.json')))
    assert len(data) == 4, 'Incorrect rows number'
    assert data[0] == {
            'name': 'Gilbert', 'Session': '2013', 'Score': 24, 'Completed': True
        }, 'Incorrect data row'

def test_formats_jsonline_load_file_not_found():
    ''' test formats jsonline file not found
    '''
    with pytest.raises(IOError):
        list(JSONLineFile.load(pathlib.Path('tests/resources/no-file.json')))

