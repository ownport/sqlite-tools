''' The collection of utilities for SQLite Database
'''
import pathlib
import sqlite3

from typing import List, Union

from sqlite_keeper.connection import Connection
from sqlite_keeper.table import Table
from sqlite_keeper.view import View


class Database():
    ''' Wrapper for a SQLite database connection that adds a variety of
        useful utility methods.
    '''
    def __init__(self, filename_or_conn: Union[str, pathlib.Path, sqlite3.Connection],
                        recreate: bool = False):
        ''' database init

        - `filename_or_conn` - String path to a file, or a `pathlib.Path` object, or a
        `sqlite3` connection
        - `recreate` - set to `True` to delete and recreate a file database (**dangerous**)
        '''
        self.conn = Connection(filename_or_conn, recreate)

    def __getitem__(self, name: str) -> Union["Table", "View"]:
        ''' `db[name]` returns a :class:`.Table` object for the table with the specified name.
        If the table does not exist yet it will be created the first time data is inserted into it.
        '''
        _class = None
        if name in [t[0] for t in self.tables]:
            _class = Table
        elif name in [v[0] for v in self.views]:
            _class = View
        else:
            raise RuntimeError(f'Unknown table/view type, {name}')
        return _class(self.conn, name)

    def attach(self, alias: str, filepath: Union[str, pathlib.Path]):
        ''' Attach another SQLite database file to this connection with the specified alias,
            equivalent to::

            ATTACH DATABASE 'filepath.db' AS alias
        '''
        path = str(pathlib.Path(filepath).resolve())
        sql = f"ATTACH DATABASE '{path}' AS [{alias}];"
        self.conn.execute(sql)

    # TODO check return type
    @property
    def tables(self) -> List[str]:
        ''' A list of string table names in this database.
        '''
        sql = "SELECT name, sql FROM sqlite_master WHERE type = 'table'"
        return [
            r for r in self.conn.execute(sql).fetchall()
        ]

    # TODO check return type
    @property
    def views(self) -> List[str]:
        ''' A list of string view names in this database.
        '''
        sql = "SELECT name, sql FROM sqlite_master WHERE type = 'view'"
        return [
            r for r in self.conn.execute(sql).fetchall()
        ]

    @property
    def triggers(self) -> List[str]:
        ''' A list of ``(name, table_name, sql)`` tuples representing triggers in this database
        '''
        sql = "select name, tbl_name, sql from sqlite_master where type = 'trigger'"
        return [
            r for r in self.conn.execute(sql).fetchall()
        ]

    @property
    def indexes(self) -> List[str]:
        ''' A list of ``(name, table_name, sql)`` tuples representing indexes in this database
        '''
        sql = "select name, tbl_name, sql from sqlite_master where type = 'index'"
        return [
            r for r in self.conn.execute(sql).fetchall()
        ]

    @property
    def schema(self) -> str:
        ''' SQL schema for this database
        '''
        schema = []
        sql = "SELECT sql FROM sqlite_master WHERE sql IS NOT NULL"
        rows = self.conn.execute(sql).fetchall()
        for row in rows:
            sql = row[0]
            if not sql.strip().endswith(";"):
                sql += ";"
            schema.append(sql)
        return schema
