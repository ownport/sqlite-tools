
import os
import pathlib
import sqlite3

from typing import Union, Optional, Iterable, Generator
from typing import List, Tuple

from sqlite_keeper.utils import dict_factory

def connection_factory(filename_or_conn: Union[str, pathlib.Path, sqlite3.Connection],
                        recreate: bool = False):
    '''
        - ``filename_or_conn`` - String path to a file, or a ``pathlib.Path`` object, or a
        ``sqlite3`` connection
        - ``recreate`` - set to ``True`` to delete and recreate a file database (**dangerous**)
    '''
    assert filename_or_conn is not None, "Please specify a filename_or_conn parameter in get_connection()"

    conn = None
    if filename_or_conn == ":memory:":
        conn = sqlite3.connect(":memory:")
    elif isinstance(filename_or_conn, sqlite3.Connection):
        conn = filename_or_conn
    elif isinstance(filename_or_conn, (str, pathlib.Path)):
        if recreate and os.path.exists(filename_or_conn):
            os.remove(filename_or_conn)
        conn = sqlite3.connect(str(filename_or_conn))
        
    assert isinstance(conn, sqlite3.Connection), "Cannot get sqlite connection object"
    return conn


class Connection:

    def __init__(self, 
                    filename_or_conn: Union[str, pathlib.Path, sqlite3.Connection],
                    recreate: bool = False):
        ''' returns sqlite connection object

        - ``filename_or_conn`` - String path to a file, or a ``pathlib.Path`` object, or a
        ``sqlite3`` connection
        - ``recreate`` - set to ``True`` to delete and recreate a file database (**dangerous**)

        '''
        self.conn = connection_factory(filename_or_conn, recreate)
        # self.conn.row_factory = dict_factory

    def execute(self, sql: str, 
                parameters: Optional[Union[Iterable, dict]] = None) -> sqlite3.Cursor:
        ''' Execute SQL query and return a ``sqlite3.Cursor``.
        '''
        if parameters is not None:
            return self.conn.execute(sql, parameters)
        else:
            return self.conn.execute(sql)

    def execute_many(self, sql: str, parameters: Iterable) -> sqlite3.Cursor:
        ''' Execute SQL query and return a ``sqlite3.Cursor``.
        '''
        return self.conn.executemany(sql, parameters)

    def execute_script(self, sql: str) -> sqlite3.Cursor:
        ''' Execute multiple SQL statements separated by ; and 
            return the ``sqlite3.Cursor``. 
        '''
        return self.conn.executescript(sql)

    def query(self, 
                sql: str, 
                params: Optional[Union[Iterable, dict]] = None,
                result_as_dict: bool = False) -> Generator[dict, None, None]:
        ''' Execute ``sql`` and return an iterable of dictionaries 
            representing each row.
        '''
        cursor = self.conn.execute(sql, params or tuple())
        columns = [d[0] for d in cursor.description]
        for row in cursor:
            if result_as_dict:
                yield dict(zip(columns, row))
            else:
                yield row

    def commit(self):
        ''' commit
        '''
        self.conn.commit()
