''' The module for working with CSV/TSV files
'''

import io
import csv
import pathlib

from typing import Optional, Generator
from typing import Dict, List, Tuple


class CSVFile:
    ''' The collection of methods for processing files in CSV format
    '''
    @staticmethod
    def load(path: pathlib.Path, header:bool = False,
            dialect: csv.Dialect = csv.excel,
            encoding:Optional[str] = 'utf-8-sig') -> Generator[Dict, List, Tuple]:
        ''' load data in CSV format
        '''
        if not path.is_file():
            raise IOError(f'File not found, {path}')

        with io.TextIOWrapper(path.open('rb'), encoding=encoding) as decoded_fp:
            if header:
                reader = csv.DictReader(decoded_fp, dialect=dialect)
            else:
                reader = csv.reader(decoded_fp, dialect=dialect)

            for row in reader:
                yield row
            # if dialect is not None:
            #     reader = csv.DictReader(decoded_fp, dialect=dialect)

    @staticmethod
    def save(data, path: pathlib.Path):
        ''' save data in JSONLine format
        '''

class TSVFile:
    ''' The collection of methods for processing files in TSV format
    '''
    @staticmethod
    def load(path: pathlib.Path, header:bool = False,
            encoding:Optional[str] = 'utf-8-sig'):
        ''' load data in TSV format
        '''
        return CSVFile.load(path, header = header,
                            dialect=csv.excel_tab,
                            encoding=encoding)

    @staticmethod
    def save(data, path: pathlib.Path):
        ''' save data in JSONLine format
        '''
