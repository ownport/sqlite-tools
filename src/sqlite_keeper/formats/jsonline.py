'''  The module for working with files in JSONline format
'''
import json
import pathlib


class JSONLineFile:
    ''' The collection of methods for working with JSONline format
    '''
    @staticmethod
    def load(path: pathlib.Path):
        ''' load data in JSONLine format
        '''
        if not path.is_file():
            raise IOError(f'File not found, {path}')

        for row in path.open('rb'):
            if row.strip():
                yield json.loads(row)

    @staticmethod
    def save(data, path: pathlib.Path):
        ''' save data in JSONLine format
        '''
