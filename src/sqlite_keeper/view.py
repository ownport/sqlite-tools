
from sqlite_keeper.queryable import Queryable

class View(Queryable):
    def exists(self):
        return True

    def __repr__(self):
        return "<View {}({})>".format(
            self.name, ",".join(c.name for c in self.columns)
        )

    def drop(self, ignore=False):
        try:
            self.db.execute("DROP VIEW [{}]".format(self.name))
        except sqlite3.OperationalError:
            if not ignore:
                raise

    def enable_fts(self, *args, **kwargs):
        raise NotImplementedError(
            "enable_fts() is supported on tables but not on views"
        )
