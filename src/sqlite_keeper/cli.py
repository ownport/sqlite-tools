''' Comman line interface to SQLite Keeper
'''
import logging
import argparse

from sqlite_keeper.commands.imports import add_import_arguments


logger = logging.getLogger(__name__)


def launch_new_instance():
    ''' launch new CLI instance
    '''
    parser = argparse.ArgumentParser('sqlite-keeper')
    subparsers = parser.add_subparsers()

    # Common parser
    parser.add_argument('-l', '--log-level',
                        default='INFO',
                        help='Log level: DEBUG, INFO, WARNING, ERROR, CRITICAL')
    parser.add_argument('--settings', type=str, default='.sqlite-keeper.yml',
                        help='The path to settings file, default: .sqlite-keeper.yml')

    # Import commands
    add_import_arguments(
        subparsers.add_parser('import', help='Import commands'))

    # Main
    args = parser.parse_args()

    logging.basicConfig(level=args.log_level,
                        format="%(asctime)s.%(msecs)03d (%(name)s) [%(levelname)s] %(message)s",
                        datefmt='%Y-%m-%dT%H:%M:%S')

    if hasattr(args, 'handler'):
        args.handler(args)
    else:
        parser.print_help()

if __name__ == '__main__':
    launch_new_instance()
