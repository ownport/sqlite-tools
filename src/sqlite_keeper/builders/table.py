
from sqlite_keeper.connection import Connection
from sqlite_keeper.queryable import Queryable

class Table(Queryable):
    pass

#     def create_table_sql(
#         self,
#         name: str,
#         columns: Dict[str, Any],
#         pk: Optional[Any] = None,
#         foreign_keys: Optional[ForeignKeysType] = None,
#         column_order: Optional[List[str]] = None,
#         not_null: Iterable[str] = None,
#         defaults: Optional[Dict[str, Any]] = None,
#         hash_id: Optional[str] = None,
#         extracts: Optional[Union[Dict[str, str], List[str]]] = None,
#         if_not_exists: bool = False,
#     ) -> str:
#         "Returns the SQL ``CREATE TABLE`` statement for creating the specified table."
#         foreign_keys = self.resolve_foreign_keys(name, foreign_keys or [])
#         foreign_keys_by_column = {fk.column: fk for fk in foreign_keys}
#         # any extracts will be treated as integer columns with a foreign key
#         extracts = resolve_extracts(extracts)
#         for extract_column, extract_table in extracts.items():
#             if isinstance(extract_column, tuple):
#                 assert False
#             # Ensure other table exists
#             if not self[extract_table].exists():
#                 self.create_table(extract_table, {"id": int, "value": str}, pk="id")
#             columns[extract_column] = int
#             foreign_keys_by_column[extract_column] = ForeignKey(
#                 name, extract_column, extract_table, "id"
#             )
#         # Soundness check not_null, and defaults if provided
#         not_null = not_null or set()
#         defaults = defaults or {}
#         assert all(
#             n in columns for n in not_null
#         ), "not_null set {} includes items not in columns {}".format(
#             repr(not_null), repr(set(columns.keys()))
#         )
#         assert all(
#             n in columns for n in defaults
#         ), "defaults set {} includes items not in columns {}".format(
#             repr(set(defaults)), repr(set(columns.keys()))
#         )
#         validate_column_names(columns.keys())
#         column_items = list(columns.items())
#         if column_order is not None:

#             def sort_key(p):
#                 return column_order.index(p[0]) if p[0] in column_order else 999

#             column_items.sort(key=sort_key)
#         if hash_id:
#             column_items.insert(0, (hash_id, str))
#             pk = hash_id
#         # Soundness check foreign_keys point to existing tables
#         for fk in foreign_keys:
#             if not any(
#                 c for c in self[fk.other_table].columns if c.name == fk.other_column
#             ):
#                 raise AlterError(
#                     "No such column: {}.{}".format(fk.other_table, fk.other_column)
#                 )

#         column_defs = []
#         # ensure pk is a tuple
#         single_pk = None
#         if isinstance(pk, list) and len(pk) == 1 and isinstance(pk[0], str):
#             pk = pk[0]
#         if isinstance(pk, str):
#             single_pk = pk
#             if pk not in [c[0] for c in column_items]:
#                 column_items.insert(0, (pk, int))
#         for column_name, column_type in column_items:
#             column_extras = []
#             if column_name == single_pk:
#                 column_extras.append("PRIMARY KEY")
#             if column_name in not_null:
#                 column_extras.append("NOT NULL")
#             if column_name in defaults and defaults[column_name] is not None:
#                 column_extras.append(
#                     "DEFAULT {}".format(self.quote(defaults[column_name]))
#                 )
#             if column_name in foreign_keys_by_column:
#                 column_extras.append(
#                     "REFERENCES [{other_table}]([{other_column}])".format(
#                         other_table=foreign_keys_by_column[column_name].other_table,
#                         other_column=foreign_keys_by_column[column_name].other_column,
#                     )
#                 )
#             column_defs.append(
#                 "   [{column_name}] {column_type}{column_extras}".format(
#                     column_name=column_name,
#                     column_type=COLUMN_TYPE_MAPPING[column_type],
#                     column_extras=(" " + " ".join(column_extras))
#                     if column_extras
#                     else "",
#                 )
#             )
#         extra_pk = ""
#         if single_pk is None and pk and len(pk) > 1:
#             extra_pk = ",\n   PRIMARY KEY ({pks})".format(
#                 pks=", ".join(["[{}]".format(p) for p in pk])
#             )
#         columns_sql = ",\n".join(column_defs)
#         sql = """CREATE TABLE {if_not_exists}[{table}] (
# {columns_sql}{extra_pk}
# );
#         """.format(
#             if_not_exists="IF NOT EXISTS " if if_not_exists else "",
#             table=name,
#             columns_sql=columns_sql,
#             extra_pk=extra_pk,
#         )
#         return sql

#     def create_table(
#         self,
#         name: str,
#         columns: Dict[str, Any],
#         pk: Optional[Any] = None,
#         foreign_keys: Optional[ForeignKeysType] = None,
#         column_order: Optional[List[str]] = None,
#         not_null: Iterable[str] = None,
#         defaults: Optional[Dict[str, Any]] = None,
#         hash_id: Optional[str] = None,
#         extracts: Optional[Union[Dict[str, str], List[str]]] = None,
#         if_not_exists: bool = False,
#     ) -> "Table":
#         """
#         Create a table with the specified name and the specified ``{column_name: type}`` columns.

#         See :ref:`python_api_explicit_create`.
#         """
#         sql = self.create_table_sql(
#             name=name,
#             columns=columns,
#             pk=pk,
#             foreign_keys=foreign_keys,
#             column_order=column_order,
#             not_null=not_null,
#             defaults=defaults,
#             hash_id=hash_id,
#             extracts=extracts,
#             if_not_exists=if_not_exists,
#         )
#         self.execute(sql)
#         table = self.table(
#             name,
#             pk=pk,
#             foreign_keys=foreign_keys,
#             column_order=column_order,
#             not_null=not_null,
#             defaults=defaults,
#             hash_id=hash_id,
#         )
#         return cast(Table, table)
