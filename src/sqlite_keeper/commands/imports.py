''' The CLI module for importing data into sqlite database
'''
import logging

from argparse import ArgumentParser

logger = logging.getLogger(__name__)

def add_import_arguments(parser: ArgumentParser):
    ''' add import paramaters
    '''
    parser.add_argument('--source', type=str, required=True,
            help='data source: JSON and csv file (path to file) or stdin (-)')
    parser.add_argument('--target', type=str, required=True,
            help='target: database and table in the format: <database>.<table>')
    parser.set_defaults(handler=handle_cli_commands)

    return parser


def handle_cli_commands(args):
    ''' CLI Handler for import command
    '''
    source = args.source
    target = args.target
    logger.info('Source: %s', source)
    logger.info('Target: %s', target)
    # logger.warning('No action required, use --help to get more details')
