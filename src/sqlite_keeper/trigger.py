
from collections import namedtuple


Trigger = namedtuple("Trigger", ("name", "table", "sql"))
